import json


class WordStats(object):
    def __init__(self, word='', confidence=0, amount=1):
        if word in ['x', '*']:
            word = 'multiply'
        self.word = word
        self.confidence = confidence
        self.amount = amount

    def to_json(self):
        return json.dumps(self, default=lambda o: o.__dict__,
                          sort_keys=True, indent=4)

    @classmethod
    def from_json(cls, json_data):
        if json_data is None:
            return None
        return cls(**json_data)
