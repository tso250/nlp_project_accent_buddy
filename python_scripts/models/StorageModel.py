import json

from SoundFeatures import SoundFeatures
from models.PersonStats import PersonStats


class Storage(object):
    def __init__(self, google={}, sphinks={}, sound_files_features={}, persons_stats=[], stats={}):
        """

        :param google:
        :param sphinks:
        :param sound_files_features:
        :param persons_stats:
        """
        self.stats = stats
        self.persons_stats = persons_stats
        self.google = google
        self.sphinks = sphinks
        self.sound_files_features = sound_files_features

    @classmethod
    def from_json(cls, json_data):
        if json_data is None:
            return None

        if "stats" not in json_data:
            json_data["stats"] = {}

        if "google" not in json_data:
            json_data["google"] = {}

        if "sphinks" not in json_data:
            json_data["sphinks"] = {}

        if "sound_files_features" in json_data:
            sound_files_features_dict = json_data["sound_files_features"]
            json_data["sound_files_features"] = {key: SoundFeatures.from_json(sound_files_features_dict[key])
                                                 for key in sound_files_features_dict}
        else:
            json_data["sound_files_features"] = {}

        if "persons_stats" in json_data:
            json_data["persons_stats"] = list(map(PersonStats.from_json, json_data["persons_stats"]))
        else:
            json_data["persons_stats"] = []

        return cls(**json_data)

    def to_json(self):
        return json.dumps(self, default=lambda o: o.__dict__,
                          sort_keys=True, indent=4)

