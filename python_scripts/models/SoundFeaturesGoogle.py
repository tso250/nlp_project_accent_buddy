import json

from WordStats import WordStats


def clean_data(word):
    if word in ['x', '*']:
        word = 'multiply'
    return word


class SoundFeaturesGoogle(object):
    def __init__(self, words_list=[], words_stats=[], substitutes=[]):
        self.words_list = words_list
        self.words_list = [clean_data(word) for word in words_list]
        self.words_stats = words_stats
        self.substitutes = substitutes

    def to_json(self):
        return json.dumps(self, default=lambda o: o.__dict__,
                          sort_keys=True, indent=4)

    @classmethod
    def from_json(cls, json_data):
        if json_data is None:
            return None

        words_list = []
        if 'words_list' in json_data:
            words_list = json_data['words_list']
            words_list = [clean_data(word) for word in words_list]

        words_stats = []
        if 'words_stats' in json_data:
            words_stats_list = json_data['words_stats']
            words_stats = list(map(WordStats.from_json, words_stats_list))

        substitutes = []
        if 'substitutes' in json_data:
            substitutes = json_data['substitutes']
        return cls(words_list, words_stats, substitutes)


