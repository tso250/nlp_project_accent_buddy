import json

from SoundFeaturesGoogle import SoundFeaturesGoogle
from SoundFeaturesPocketSphinks import SoundFeaturesPocketSphinks
from WordStats import WordStats
from statistics.phrases_data import get_phrase_id_by_sentence_to_say


class SoundFeatures(object):
    def __init__(self, is_valid=False, file_name=''
                 , googleSoundFeatures=None
                 , pocketSphinksSoundFeatures=None
                 , wordsStats=[]
                 , sentence_to_say=''
                 , modified_time=''):

        if googleSoundFeatures is None:
            googleSoundFeatures = SoundFeaturesGoogle(words_list=[], words_stats=[], substitutes=[])

        if pocketSphinksSoundFeatures is None:
            pocketSphinksSoundFeatures = SoundFeaturesPocketSphinks(words_stats=[]
                                                                    , phonemes_stats=[]
                                                                    , phonemes_model_best_score=0
                                                                    , phonemes_model_confidence=0
                                                                    , phonemes_hyp_str=''
                                                                    , phonemes_list=[]
                                                                    , words_model_best_score=0
                                                                    , words_model_confidence=0
                                                                    , words_hyp_str=''
                                                                    , words_list=[]
                                                                    )

        self.is_valid = is_valid
        self.file_name = file_name
        self.googleSoundFeatures = googleSoundFeatures
        self.pocketSphinksSoundFeatures = pocketSphinksSoundFeatures
        self.wordsStats = wordsStats
        self.sentence_to_say = sentence_to_say
        self.modified_time = modified_time

    def get_phrase_id(self):
        return get_phrase_id_by_sentence_to_say(self.sentence_to_say)

    def to_json(self):
        return json.dumps(self, default=lambda o: o.__dict__,
                          sort_keys=True, indent=4)

    @classmethod
    def from_json(cls, json_data):
        if json_data is None:
            return None

        is_valid = False
        file_name = ''
        google_sound_features = None
        pocket_sphinks_sound_features = None
        words_stats = []
        sentence_to_say = ''
        modified_time = ''

        if 'is_valid' in json_data:
            is_valid = json_data['is_valid']

        if 'file_name' in json_data:
            file_name = json_data['file_name']

        if 'googleSoundFeatures' in json_data:
            google_sound_features = SoundFeaturesGoogle.from_json(json_data['googleSoundFeatures'])

        if 'pocketSphinksSoundFeatures' in json_data:
            pocket_sphinks_sound_features = SoundFeaturesPocketSphinks.from_json(json_data['pocketSphinksSoundFeatures'])

        if 'wordsStats' in json_data:
            wordsStatsLst = json_data['wordsStats']
            words_stats = list(map(WordStats.from_json, wordsStatsLst))

        if 'sentence_to_say' in json_data:
            sentence_to_say = json_data['sentence_to_say']

        if 'modified_time' in json_data:
            modified_time = json_data['modified_time']

        return cls(is_valid
                   , file_name
                   , google_sound_features
                   , pocket_sphinks_sound_features
                   , words_stats
                   , sentence_to_say
                   , modified_time)
