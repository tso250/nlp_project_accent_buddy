import json

from WordStats import WordStats


class SoundFeaturesPocketSphinks:
    def __init__(self
                 , words_stats=[]
                 , phonemes_stats=[]

                 , phonemes_model_best_score=0
                 , phonemes_model_confidence=0
                 , phonemes_hyp_str=''
                 , phonemes_list= []

                 , words_model_best_score=0
                 , words_model_confidence=0
                 , words_hyp_str=''
                 , words_list=[]
                 ):
        self.words_stats = words_stats
        self.phonemes_stats = phonemes_stats

        self.phonemes_model_best_score = phonemes_model_best_score
        self.phonemes_model_confidence = phonemes_model_confidence
        self.phonemes_hyp_str = phonemes_hyp_str
        self.phonemes_list = phonemes_list

        self.words_model_best_score = words_model_best_score
        self.words_model_confidence = words_model_confidence
        self.words_hyp_str = words_hyp_str
        self.words_list = words_list

    def to_json(self):
        return json.dumps(self, default=lambda o: o.__dict__,
                          sort_keys=True, indent=4)

    @classmethod
    def from_json(cls, json_data):
        if json_data is None:
            return None

        words_stats = []

        phonemes_stats = []

        phonemes_model_best_score = 0
        phonemes_model_confidence = 0
        phonemes_hyp_str = ''
        phonemes_list = ''

        words_model_best_score = 0
        words_model_confidence = 0
        words_hyp_str = ''
        words_list = []

        if 'words_stats' in json_data:
            words_stats_list = json_data['words_stats']
            words_stats = list(map(WordStats.from_json, words_stats_list))

        if 'words_stats' in json_data:
            phonemes_stats_list = json_data['phonemes_stats']
            phonemes_stats = list(map(WordStats.from_json, phonemes_stats_list))

        if 'phonemes_model_best_score' in json_data:
            phonemes_model_best_score = json_data['phonemes_model_best_score']
        if 'phonemes_model_confidence' in json_data:
            phonemes_model_confidence = json_data['phonemes_model_confidence']
        if 'phonemes_hyp_str' in json_data:
            phonemes_hyp_str = json_data['phonemes_hyp_str']
        if 'phonemes_list' in json_data:
            phonemes_list = json_data['phonemes_list']

        if 'words_model_best_score' in json_data:
            words_model_best_score = json_data['words_model_best_score']
        if 'words_model_confidence' in json_data:
            words_model_confidence = json_data['words_model_confidence']
        if 'words_hyp_str' in json_data:
            words_hyp_str = json_data['words_hyp_str']
        if 'words_list' in json_data:
            words_list = json_data['words_list']

        return cls(words_stats, phonemes_stats
                   , phonemes_model_best_score
                   , phonemes_model_confidence
                   , phonemes_hyp_str
                   , phonemes_list
                   , words_model_best_score
                   , words_model_confidence
                   , words_hyp_str
                   , words_list)


