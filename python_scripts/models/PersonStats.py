import json
import logging

from models.SoundFeatures import SoundFeatures
from statistics.phrases_data import PHRASE_1_CAN_ID, PHRASE_2_SQUARE_ID, PHRASE_3_I_ID, phrase_to_say_1_simple, \
    phrase_to_say_2_simple, phrase_to_say_3_simple

logger = logging.getLogger('root')
SHOW_DETAILS = True


def beautify(collection):
    beauty = "\n"
    for item in collection:
        beauty += "\t\t*" + str(item) + "*\n"
    return beauty


def beautify_phoneme_stats(phoneme_stats):
    beauty = "\n"
    try:
        for phrase_id in phoneme_stats:
            phrase_stats = phoneme_stats[phrase_id]
            beauty += "\n\t#\t\t{}\t\n".format(phrase_stats["phrase"])
            score_phrase = phrase_stats["phonemes_score_by_phrase"]
            beauty += "\t#\t\tPhrase simple phonemes score # {}% #\t\n".format(round(score_phrase["score"], 1))
            score_training = phrase_stats["phonemes_score_by_training_set"]
            beauty += "\t#\t\tTraining set phonemes score # {}% #\t\n".format(round(score_training["score"], 1))

            if "total_phonemes_score" in phrase_stats:
                total_score = phrase_stats["total_phonemes_score"]
                beauty += "\t#\t\tTotal phrase phonemes score # {}% #\t\n".format(round(total_score, 1))
            else:
                logger.error("bad stats: {}".format(str(phrase_stats)))

            score_word = phrase_stats["phonemes_score_by_word"]
            for word_key in score_word:
                word_data = score_word[word_key]
                beauty += "\n\t*" + word_key + "*\n"
                beauty += "\t\tWord '{}' phoneme score: # {}% #\n".format(word_key, round(float(word_data["score"]), 2))

                missing = len(word_data["missing_phonemes"]) > 0
                if missing:
                    beauty += "\t\tMissing Phonemes: "
                    for missing_phoneme in word_data["missing_phonemes"]:
                        beauty += "{} ".format(missing_phoneme)

                if missing:
                    beauty += "\n\t\tExpected Phonemes: "
                    for expected_phoneme in word_data["expected_phonemes"]:
                        beauty += "{} ".format(expected_phoneme)
                    beauty += "\n"
    except Exception as e:
        logger.error(e)

    beauty += "\n"
    return beauty


def beautify_score_phrase_stats(person_stat):
    beauty = "\n"
    try:
        phrases_score = person_stat.phrases_stats['Can they do both, work and study?']
        beauty += "\t\t'" + phrase_to_say_1_simple + "' got: # {}% #\n".format(round(phrases_score, 2))
        phrases_score = person_stat.phrases_stats['Square area is width multiply by height!']
        beauty += "\t\t'" + phrase_to_say_2_simple + "' got: # {}% #\n".format(round(phrases_score, 2))
        phrases_score = person_stat.phrases_stats["I can't drink cold water when my teeth hurt."]
        beauty += "\t\t'" + phrase_to_say_3_simple + "' got: # {}% #\n".format(round(phrases_score, 2))
    except Exception as e:
        logger.error(e)
    return beauty


def beautify_words_stats(person_stat):
    beauty = ""
    try:
        beauty = "\n"
        beauty += "\t\tGoogle thinks you said:\n\t\t{\n"
        google_subs = person_stat.words_stats['Google_subs']
        sphinks_subs = person_stat.words_stats['Sphinks_subs']

        for i in range(len(google_subs)):
            beauty += "\t\t\t'"
            beauty += google_subs[i]['sub']
            beauty += "' instead of: '" + google_subs[i]['word']
            beauty += "'\n"
        beauty += '\t\t}\n\n'
        beauty += "\t\tSphinks thinks you said:\n\t\t{\n"
        for i in range(len(sphinks_subs)):
            beauty += "\t\t\t'"
            beauty += sphinks_subs[i]['sub']
            beauty += "' instead of: '" + sphinks_subs[i]['word']
            beauty += "'\n"
        beauty += '\t\t}\n\n'
    except Exception as e:
        logger.error(e)
    return beauty


class PersonStats(object):
    def __init__(self,
                 person_name='',
                 uid='',
                 total_person_score=0,
                 detected_sentences_to_improve=[],
                 detected_words_to_improve=[],
                 detected_phonemes_substitutes=[],
                 phrases_stats={},
                 words_stats={},
                 phonemes_stats={},
                 sound_features_phrase_1=None,
                 sound_features_phrase_2=None,
                 sound_features_phrase_3=None,
                 phrases_by_id=None
                 ):

        self.sound_features_phrase_1 = sound_features_phrase_1
        self.sound_features_phrase_2 = sound_features_phrase_2
        self.sound_features_phrase_3 = sound_features_phrase_3
        self.person_name = person_name
        self.uid = uid
        self.total_person_score = total_person_score
        self.detected_sentences_to_improve = detected_sentences_to_improve
        self.detected_words_to_improve = detected_words_to_improve
        self.detected_phonemes_substitutes = detected_phonemes_substitutes
        self.phrases_stats = phrases_stats
        self.words_stats = words_stats
        self.phonemes_stats = {int(key): phonemes_stats[key] for key in phonemes_stats}

    def to_json(self):
        return json.dumps(self, default=lambda o: o.__dict__,
                          sort_keys=True, indent=4)

    def __str__(self):
        str_to_print = ""
        try:
            str_to_print += "Person name:  {} \n".format(self.person_name)
            str_to_print += "\tTotal English Accent Score: # {}% #\n".format(round(float(self.total_person_score), 2))
            str_to_print += "\tDetected Phrases To Improve: {}\n".format(beautify(self.detected_sentences_to_improve))
            str_to_print += "\tDetected Words To Improve: {}\n".format(beautify(self.detected_words_to_improve))
            str_to_print += "\tDetected Phonemes To Improve: {}\n".format(beautify(self.detected_phonemes_substitutes))
            if SHOW_DETAILS:
                str_to_print += "\tEnglish Accent Score By Phrase:\n{}".format(beautify_score_phrase_stats(self))
                str_to_print += "\t{}\n".format(beautify_words_stats(self))
                str_to_print += "\tEnglish Accent Score By Phonemes: {}\n\n".format(beautify_phoneme_stats(self.phonemes_stats))
        except Exception as e:
            logger.error(e)
        str_to_print += "\n\n"
        return str_to_print

    @classmethod
    def from_json(cls, json_data):
        if json_data is None:
            return None

        if 'sound_features_phrase_1' in json_data:
            json_data['sound_features_phrase_1'] = SoundFeatures.from_json(json_data['sound_features_phrase_1'])
        if 'sound_features_phrase_2' in json_data:
            json_data['sound_features_phrase_2'] = SoundFeatures.from_json(json_data['sound_features_phrase_2'])
        if 'sound_features_phrase_3' in json_data:
            json_data['sound_features_phrase_3'] = SoundFeatures.from_json(json_data['sound_features_phrase_3'])

        personStats = cls(**json_data)
        personStats.phrases_by_id = {
            PHRASE_1_CAN_ID: personStats.sound_features_phrase_1,
            PHRASE_2_SQUARE_ID: personStats.sound_features_phrase_2,
            PHRASE_3_I_ID: personStats.sound_features_phrase_3,
        }
        return personStats

    def get_phrase_features_by_id(self, phrase_id):
        """

        :param phrase_id:
        :return: return the sound features of the person specific phrase
        :rtype: models.SoundFeatures.SoundFeatures
        """
        self.phrases_by_id = {
            PHRASE_1_CAN_ID: self.sound_features_phrase_1,
            PHRASE_2_SQUARE_ID: self.sound_features_phrase_2,
            PHRASE_3_I_ID: self.sound_features_phrase_3,
        }
        return self.phrases_by_id[phrase_id]



