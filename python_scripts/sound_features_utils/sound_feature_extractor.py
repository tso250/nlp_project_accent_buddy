import logging
import os

import speech_recognition as sr
from pocketsphinx import Decoder
from models.SoundFeatures import SoundFeatures
from models.SoundFeaturesGoogle import SoundFeaturesGoogle
from models.SoundFeaturesPocketSphinks import SoundFeaturesPocketSphinks
from models.WordStats import WordStats
from sound_features_utils.hmm_model_settings import get_hmm_model_decoder_default_config, \
    get_hmm_model_decoder_phonemes_config
from storage.storage_settings import sounds_dir
from utils.audio_helper import is_wav_file_valid_and_get_frame_rate, get_wav_file_audio_data

logger = logging.getLogger('root')


def get_decoder_with_extracted_features(config, audio_file_raw_data):
    decoder = Decoder(config)
    decoder.start_utt()
    decoder.process_raw(audio_file_raw_data, False, True)
    decoder.end_utt()

    if decoder.hyp() is None:
        return None

    return decoder


def get_pocket_sphinks_decoder_with_phonemes_data(audio_file_raw_data):
    config = get_hmm_model_decoder_phonemes_config()
    decoder = get_decoder_with_extracted_features(config, audio_file_raw_data)
    return decoder


def get_pocket_sphinks_decoder_with_words_data(audio_file_raw_data):
    config = get_hmm_model_decoder_default_config()
    decoder = get_decoder_with_extracted_features(config, audio_file_raw_data)
    return decoder


def extract_sound_features_pocket_sphinks(audio_file_raw_data):
    pocket_sphinks_sound_features = SoundFeaturesPocketSphinks(words_stats=[]
                                                               , phonemes_stats=[]
                                                               , phonemes_model_best_score=0
                                                               , phonemes_model_confidence=0
                                                               , phonemes_hyp_str=''
                                                               , phonemes_list=[]
                                                               , words_model_best_score=0
                                                               , words_model_confidence=0
                                                               , words_hyp_str=''
                                                               , words_list=[]
                                                               )
    try:
        decoder = get_pocket_sphinks_decoder_with_phonemes_data(audio_file_raw_data)
        if decoder is None:
            return None

        logmath_phonemes = decoder.get_logmath()
        phonemes_hyp = decoder.hyp()
        phonemes_hyp_str = phonemes_hyp.hypstr
        phonemes_model_confidence = logmath_phonemes.exp(phonemes_hyp.prob)
        phonemes_model_best_score = phonemes_hyp.best_score

        for seg in decoder.seg():
            word = seg.word

            if '<' in word:
                continue

            if '+' in word:
                continue

            if 'SIL' in word:
                continue

            if '(' in word:
                end_index = word.find('(')
                word = word[:end_index]

            word_stats = WordStats(word='', confidence=0, amount=1)
            word_stats.word = word
            word_stats.confidence = logmath_phonemes.exp(seg.prob)
            pocket_sphinks_sound_features.phonemes_list.append(word)
            pocket_sphinks_sound_features.phonemes_stats.append(word_stats)

        pocket_sphinks_sound_features.phonemes_hyp_str = phonemes_hyp_str
        pocket_sphinks_sound_features.phonemes_model_best_score = phonemes_model_best_score
        pocket_sphinks_sound_features.phonemes_model_confidence = phonemes_model_confidence

        decoder = get_pocket_sphinks_decoder_with_words_data(audio_file_raw_data)
        if decoder is None:
            return None

        words_hyp = decoder.hyp()
        logmath_words = decoder.get_logmath()
        words_hyp_str = words_hyp.hypstr
        words_model_confidence = logmath_words.exp(words_hyp.prob)
        words_model_best_score = words_hyp.best_score
        for seg in decoder.seg():
            word = seg.word.lower()

            if '<' in word:
                continue

            if '[' in word:
                continue

            if '(' in word:
                end_index = word.find('(')
                word = word[:end_index]

            word_stats = WordStats(word='', confidence=0, amount=1)
            word_stats.word = word
            word_stats.confidence = logmath_words.exp(seg.prob)
            pocket_sphinks_sound_features.words_list.append(word)
            pocket_sphinks_sound_features.words_stats.append(word_stats)

        pocket_sphinks_sound_features.words_hyp_str = words_hyp_str
        pocket_sphinks_sound_features.words_model_best_score = words_model_best_score
        pocket_sphinks_sound_features.words_model_confidence = words_model_confidence

    except Exception as e:
        logger.error(e)

    return pocket_sphinks_sound_features


def extract_sound_features_google(audio_data):
    google_sound_features = SoundFeaturesGoogle(words_list=[], words_stats=[], substitutes=[])
    try:
        recognizer = sr.Recognizer()
        google_recognized_results = recognizer.recognize_google(audio_data, show_all=True)
        google_sound_features.substitutes = []

        if google_recognized_results['alternative'] is None:
            logging.warn('No Google Prediction!')
            return google_sound_features

        for prediction in google_recognized_results['alternative']:
            if prediction['transcript'] is None:
                continue
            transcript_words = prediction['transcript'].split()
            google_sound_features.substitutes.append(transcript_words)
            if 'confidence' in prediction:
                for recognized_word in transcript_words:
                    recognized_word = recognized_word.lower()
                    google_sound_features.words_list.append(recognized_word)
                    word_stats = WordStats(word='', confidence=0, amount=1)
                    word_stats.word = recognized_word
                    word_stats.confidence = prediction['confidence']
                    google_sound_features.words_stats.append(word_stats)
    except Exception as e:
        logger.error(e)
    return google_sound_features


def update_sound_features_for_sound_dir(files_sound_features, sounds_dir):
    for root, dirs, files in os.walk(sounds_dir):
        for file_name in files:
            try:
                audio_path = os.path.join(root, file_name)
                modified_time = int(os.stat(audio_path).st_mtime / 10)
                if file_name in files_sound_features:
                    existing_feature = files_sound_features[file_name]
                    existing_feature.modified_time = modified_time
                    continue

                sound_features = SoundFeatures(is_valid=False
                                               , file_name=''
                                               , googleSoundFeatures=None
                                               , pocketSphinksSoundFeatures=None
                                               , wordsStats=[]
                                               , sentence_to_say=''
                                               , modified_time=modified_time)

                sound_features.sentence_to_say = file_name.split('_')[0]
                sound_features.file_name = file_name
                logger.info("Scanning file: " + audio_path)
                valid, frame_rate = is_wav_file_valid_and_get_frame_rate(audio_path)
                if valid:
                    logger.info("Adding sound statistics to to DB")
                    sound_features.is_valid = True
                    (audio_data, audio_raw_data) = get_wav_file_audio_data(audio_path, frame_rate)
                    sound_features.googleSoundFeatures = extract_sound_features_google(audio_data)
                    sound_features.pocketSphinksSoundFeatures = extract_sound_features_pocket_sphinks(audio_raw_data)
                else:
                    logger.warn("Skipping file - Not a valid WAV file.")
                    sound_features.pocketSphinksSoundFeatures = None
                    sound_features.googleSoundFeatures = None
                    continue
                files_sound_features[file_name] = sound_features
            except Exception as e:
                logger.error(e)


def extract_phrase_sound_features(audio_path, file_name, audio_data, audio_raw_data):
    modified_time = int(os.stat(audio_path).st_mtime / 10)
    sound_features = SoundFeatures(is_valid=False
                                   , file_name=''
                                   , googleSoundFeatures=None
                                   , pocketSphinksSoundFeatures=None
                                   , wordsStats=[]
                                   , sentence_to_say=''
                                   , modified_time=modified_time)

    sound_features.sentence_to_say = file_name.split('_')[0]
    sound_features.file_name = file_name

    sound_features.googleSoundFeatures = extract_sound_features_google(audio_data)

    sound_features.pocketSphinksSoundFeatures = extract_sound_features_pocket_sphinks(audio_raw_data)

    return sound_features





def extract_sound_features(storage):
    """
    Extract sound features with Google Speech API and CMU Pocket Sphinks HMM offline model,
            from collected sound files - that are not marked as already scanned.
    :param models.StorageModel.StorageModel storage:
    :return:
    """
    update_sound_features_for_sound_dir(storage.sound_files_features, sounds_dir)

