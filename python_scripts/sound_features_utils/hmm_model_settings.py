from pocketsphinx import Decoder

from storage.storage_settings import hmm_path, dict_path, lm_path, phoneme_path


def get_hmm_model_decoder_default_config():
    config = Decoder.default_config()
    config.set_string('-logfn', 'log.log')
    config.set_string('-hmm', hmm_path)
    config.set_string('-dict', dict_path)
    config.set_string('-lm', lm_path)
    return config


def get_hmm_model_decoder_phonemes_config():
    config = Decoder.default_config()
    config.set_string('-logfn', 'log.log')
    config.set_string('-hmm', hmm_path)
    config.set_string('-dict', dict_path)
    config.set_string('-lm', lm_path)
    config.set_string('-allphone', phoneme_path)
    return config
