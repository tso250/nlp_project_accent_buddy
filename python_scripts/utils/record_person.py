import logging
import os
import uuid
import wave
import pydub as pydub
import speech_recognition as sr
from models.PersonStats import PersonStats
from sound_features_utils.sound_feature_extractor import extract_phrase_sound_features
from statistics.phrases_data import phrase_to_say_1, phrase_to_say_2, phrase_to_say_3
from statistics.phrases_data import phrase_file_name_1, phrase_file_name_2, phrase_file_name_3
from statistics.stats_utils import calculate_person_score
from storage.storage_settings import generate_unique_sound_file_name, DATADIR, DATA_DIR_ANALYSIS, sounds_dir, \
    testing_set_dir

logger = logging.getLogger('root')


def get_and_record_sound_raw_data(phrase_to_say, audio_path):
    raw_data = None
    audio_data = None
    frame_rate = 441000
    audio_file = None

    from os import path
    # obtain audio from the microphone
    r = sr.Recognizer()
    with sr.Microphone() as source:
        print("Please wait. Calibrating microphone...")
        # listen for 5 seconds and create the ambient noise energy level
        r.adjust_for_ambient_noise(source, duration=5)
        print('...')
        print("Say: '" + phrase_to_say + "'")
        audio = r.listen(source, phrase_time_limit=6)

        uid_dir = os.path.dirname(audio_path)
        if not os.path.exists(uid_dir):
            os.makedirs(uid_dir)

        with open(audio_path, 'wb+') as current_audio_file:
            current_audio_file.write(audio.get_wav_data())

    try:
        (path, file_extension) = path.splitext(audio_path)
        file_extension_final = file_extension.replace('.', '')
        is_wav = audio_path.find('wav') > 0
        if not is_wav:
            sound = pydub.AudioSegment.from_file(audio_path, file_extension_final)
            audio_path = audio_path + ".wav"
            sound.export(audio_path, format="wav")
    except Exception as inst:
        print(inst.message)
    finally:
        pass

    try:
        audio_file = wave.open(audio_path, 'r')
        frame_rate = audio_file.getframerate()
    except Exception as inst:
        print(inst.message)
    finally:
        if audio_file:
            audio_file.close()

    with open(audio_path, 'rb') as stream:
        buf = stream.read()
        if buf:
            audio_data = sr.AudioData(buf, frame_rate, 2)
            raw_data = audio_data.get_raw_data(convert_rate=16000, convert_width=2)

    return raw_data, audio_data


def record_phrases_and_get_person_stats():
    person_stats = PersonStats()
    try:
        print("Hello, please enter your name: ")
        person_name = raw_input()
        uid = uuid.uuid4().hex
        file_name_1 = generate_unique_sound_file_name(phrase_file_name_1, uid)
        file_name_2 = generate_unique_sound_file_name(phrase_file_name_2, uid)
        file_name_3 = generate_unique_sound_file_name(phrase_file_name_3, uid)

        audio_path_1 = os.path.join(testing_set_dir, uid, file_name_1)
        audio_path_2 = os.path.join(testing_set_dir, uid, file_name_2)
        audio_path_3 = os.path.join(testing_set_dir, uid, file_name_3)

        (audio_raw_data_1, audio_data_1) = get_and_record_sound_raw_data(phrase_to_say_1, audio_path_1)
        (audio_raw_data_2, audio_data_2) = get_and_record_sound_raw_data(phrase_to_say_2, audio_path_2)
        (audio_raw_data_3, audio_data_3) = get_and_record_sound_raw_data(phrase_to_say_3, audio_path_3)

        person_stats.person_name = person_name
        person_stats.uid = uid

        print("Extracting sound features for all recordings...")
        person_stats.sound_features_phrase_1 = extract_phrase_sound_features(
            audio_path_1, file_name_1, audio_data_1, audio_raw_data_1)

        print("Finished 1 of 3")

        person_stats.sound_features_phrase_2 = extract_phrase_sound_features(
            audio_path_2, file_name_2, audio_data_2, audio_raw_data_2)

        print("Finished 2 of 3")

        person_stats.sound_features_phrase_3 = extract_phrase_sound_features(
            audio_path_3, file_name_3, audio_data_3, audio_raw_data_3)

        print("Finished 3 of 3")

        return person_stats
    except Exception as e:
        logger.error (e)

    return None


def record_testing_set_data(storage):
    print('Record new data for testing set? y - yes, n - no')
    user_answer = raw_input()
    if 'y' in user_answer:
        print("Recording will start shortly...")
        person_stats = record_phrases_and_get_person_stats()
        if person_stats is not None:
            print("Hi {}, seems like we extracted all Google and CMU Pocket Sphinx sound features :)".format(
                person_stats.person_name))
            storage.persons_stats.append(person_stats)
            print("Analyzing {} accent...".format(person_stats.person_name))
            calculate_person_score(storage, person_stats)
            print (person_stats)
