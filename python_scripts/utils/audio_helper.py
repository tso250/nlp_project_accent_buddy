import logging
import os
import speech_recognition as sr
import wave

logger = logging.getLogger('root')


def is_wav_file_valid_and_get_frame_rate(audio_path):
    audio_file = None
    try:
        (path, file_extension) = os.path.splitext(audio_path)
        file_extension_final = file_extension.replace('.', '')
        is_wav = file_extension_final == 'wav'
        if not is_wav:
            return False, 0

        audio_file = wave.open(audio_path, 'r')
        frame_rate = audio_file.getframerate()
        logger.info("audio file: " + audio_path + " frame rate: " + str(frame_rate))
        return True, frame_rate
    except Exception as e:
        logger.error(e)
        return False, 0
    finally:
        if audio_file:
            audio_file.close()


def get_wav_file_audio_data(audio_path, frame_rate):
    try:
        with open(audio_path, 'rb') as stream:
            buf = stream.read()
            if buf:
                audio_data = sr.AudioData(buf, frame_rate, 2)
                raw_data = audio_data.get_raw_data(convert_rate=16000, convert_width=2)
                return audio_data, raw_data
    except Exception as e:
        logger.error(e)
    return None, None
