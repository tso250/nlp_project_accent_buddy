# Misc:


# pip install -r requirements.txt

# CAN	K AE N
# CAN(2)	K AH N
# THEY	DH EY
# DO	D UW
# BOTH	B OW TH
# WORK	W ER K
# AND	AH N D
# AND(2)	AE N D
# STUDY	S T AH D IY


# SQUARE	S K W EH R
# AREA	EH R IY AH
# IS	IH Z
# WIDTH	W IH D TH
# MULTIPLY	M AH L T AH P L AY
# BY	B AY
# HEIGHT	HH AY T

# I 	AY
# CAN'T	K AE N T
# DRINK	D R IH NG K
# COLD	K OW L D
# WATER	W AO T ER
# WHEN	W EH N
# WHEN(2)	HH W EH N
# WHEN(3)	W IH N
# WHEN(4)	HH W IH N
# MY	M AY
# TEETH	T IY TH
# HURT	HH ER T

# AA
# AE
# AH
# AO
# AW
# AY
# B
# CH
# D
# DH
# EH
# ER
# EY
# F
# G
# HH
# IH
# IY
# JH
# K
# L
# M
# N
# NG
# OW
# OY
# P
# R
# S
# SH
# T
# TH
# UH
# UW
# V
# W
# Y
# Z
# ZH
# SIL
#################################
# AA	vowel
# AE	vowel
# AH	vowel
# AO	vowel
# AW	vowel
# AY	vowel
# B	stop
# CH	affricate
# D	stop
# DH	fricative
# EH	vowel
# ER	vowel
# EY	vowel
# F	fricative
# G	stop
# HH	aspirate
# IH	vowel
# IY	vowel
# JH	affricate
# K	stop
# L	liquid
# M	nasal
# N	nasal
# NG	nasal
# OW	vowel
# OY	vowel
# P	stop
# R	liquid
# S	fricative
# SH	fricative
# T	stop
# TH	fricative
# UH	vowel
# UW	vowel
# V	fricative
# W	semivowel
# Y	semivowel
# Z	fricative
# ZH	fricative

# print ("Pronunciation for word 'Hello' is ", decoder.lookup_word("hello"))
# print('Please enter a WAV file name: ')
# file_name = raw_input()
# audio_path = path.join(DATADIR, file_name)

# with sr.Microphone() as source:
#     print("Please wait. Calibrating microphone...")
#     # listen for 5 seconds and create the ambient noise energy level
#     r.adjust_for_ambient_noise(source, duration=4)
#     print("Say something!")
#     audio = r.listen(source, phrase_time_limit=4)
#     audio_path = path.join(DATADIR, 'current.raw')
#     with open(audio_path, 'wb+') as current_audio_file:
#         current_audio_file.write(audio.frame_data)


# decoder.start_utt()
#
# with open(audio_path, 'rb') as stream:
#     buf = stream.read()
#     audio_data = sr.AudioData(buf, 44100, 2)
#     raw_data = audio_data.get_raw_data(convert_rate=16000, convert_width=2)
#     if buf:
#         decoder.process_raw(raw_data, False, True)
# decoder.end_utt()
# import pyaudio
import audioop
#
# p = pyaudio.PyAudio()
# stream = p.open(format=pyaudio.paInt16, channels=1, rate=8000, input=True, output=True, frames_per_buffer=1024)
# stream.start_stream()


# obtain audio from the microphone
# r = sr.Recognizer()

# recognize speech using Sphinx
# try:
#     decoder = r.recognize_sphinx(audio_data, show_all=True)
#     phrase = decoder.hyp().hypstr
#     print("Sphinx thinks you said '" + phrase + "'")
# except sr.UnknownValueError:
#     print("Sphinx could not understand audio")
# except sr.RequestError as e:
#     print("Sphinx error; {0}".format(e))


# '''
# phrase_to_say_1 = 'Can they do both, work and study?'
# phrase_to_say_2 = 'Square area is width multiply by height!'
# phrase_to_say_3 = 'I can\'t drink cold water when my teeth hurt.'
#
# phrase_file_name_1 = 'can_they_do_both_work_and_study'
# phrase_file_name_2 = 'square_area_is_width_multiply_by_height'
# phrase_file_name_3 = 'i_cant_drink_water_when_my_teeth_hurt'
#
# times = {}
# def count_w(sentence):
#     for word in sentence.split():
#         if not (word in times):
#             times[word] = 1
#         else:
#             times[word]+=1
#
# count_w(phrase_to_say_1)
# count_w(phrase_to_say_2)
# count_w(phrase_to_say_3)
# for word in times:
#     print(word+" " + str(times[word]))
# '''


# from record_person import record_phrases
# # record_phrases()

#
#
# def analyse_phrase_and_save_statistics(audio_stats_path, phrase_to_say, raw_data):
#     sound_features = extract_sound_features_pocket_sphinks(raw_data)
#     print()
#     print()
#     phrase_result_str = "Sphinx thinks you said '" + sound_features.words_hyp_str + "'"
#     print(phrase_result_str)
#
#     phrase_score_str = 'Sphinx Model Score: ' \
#                        + str(sound_features.words_model_best_score) \
#                        + " confidence: " \
#                        + str(sound_features.words_model_confidence)
#
#     print(phrase_score_str)
#
#     phrase_words_str = 'Sphinx Words: ' + str(sound_features.words_list)
#     print(phrase_words_str)
#
#     print()
#     print()
#
#     phonemes_score_result_str = 'Phonemes Model Score: ' \
#                                 + str(sound_features.phonemes_model_best_score) \
#                                 + " confidence: " + str(sound_features.phonemes_model_confidence)
#
#     print(phonemes_score_result_str)
#
#     phonemes_str_result = 'Sphinx Phonemes: ' + str(sound_features.phonemes_list)
#     print(phonemes_str_result)
#
#     print()
#     print()
#     print()
#
#     with open(audio_stats_path + '.info', 'w+') as result_file:
#         result_file.write(phrase_to_say)
#         result_file.write('\n')
#         result_file.write('\n')
#         result_file.write(phrase_result_str)
#         result_file.write('\n')
#         result_file.write(phrase_score_str)
#         result_file.write('\n')
#         result_file.write(phrase_words_str)
#         result_file.write('\n')
#         result_file.write('\n')
#         result_file.write(phonemes_str_result)
#         result_file.write('\n')
#         result_file.write(phonemes_score_result_str)
#         result_file.write('\n')

# phrase_phonemes = [item for phoneme_data in expected_phrase_phonemes.values() for item in phoneme_data[0]['phonemes']]


