import logging
from statistics.phrases_data import get_expected_phrase_phonemes_by_phrase, get_expected_phrase_by_id, PHRASE_1_CAN_ID

MAX_EXTRA_PHONEMES = 10
MISSING_PHONEMES_WEIGHT_PERCENT = 0.65
PHONEMES_SCORE_BY_WORDS_WEIGHT_PERCENT = 0.25
EXTRA_PHONEMES_WEIGHT_PERCENT = 0.10
assert (MISSING_PHONEMES_WEIGHT_PERCENT + PHONEMES_SCORE_BY_WORDS_WEIGHT_PERCENT + EXTRA_PHONEMES_WEIGHT_PERCENT == 1)

logger = logging.getLogger('root')


def get_phonemes_score_training_set(storage, phrase_id, person_stats):
    """
        calculate the simplest score for phonemes,
        calculate if the actual person phonemes exist in the expected phonemes.
        TODO: ignore extra phonemes from start and end.
        TODO: weight by training - get stats for extra/missing phonemes, if the phoneme is missing
              or substitute by a different phoneme, then use weight to calculate score.
              weight by training will make score more accurate with the training set data.
              our training data has phonemes stats of Israeli English speakers.
    :param phrase_id:
    :param models.PersonStats.PersonStats person_stats:
    :return: return simple score for phonemes from 0 to 100
    :rtype: float
    """
    score = 0.0
    existing_phonemes_counter = 0
    try:
        missing_phonemes_training_set = {}
        extra_phonemes_training_set = {}
        if phrase_id not in person_stats.phonemes_stats:
            person_stats.phonemes_stats[phrase_id] = {}
        expected_phrase_data = get_expected_phrase_phonemes_by_phrase(phrase_id)
        expected_phrase_phonemes = []

        actual_phrase_data = person_stats.get_phrase_features_by_id(phrase_id)
        actual_phonemes = actual_phrase_data.pocketSphinksSoundFeatures.phonemes_list

        for word, phoneme_stats_list in expected_phrase_data.iteritems():
            phoneme_data_by_word = [phoneme_stats for phoneme_stats in phoneme_stats_list
                                    if phoneme_stats['score'] == 100][0]
            expected_phonemes_by_word = phoneme_data_by_word['phonemes']
            map(expected_phrase_phonemes.append, expected_phonemes_by_word)

        missing_phonemes = list(expected_phrase_phonemes)
        removed_phonemes = {}
        for actual_phoneme in actual_phonemes:
            if actual_phoneme in expected_phrase_phonemes:
                if actual_phoneme in removed_phonemes:
                    continue
                removed_phonemes[actual_phoneme] = True
                missing_phonemes.remove(actual_phoneme)
                existing_phonemes_counter += 1

        extra_phonemes = [actual_phoneme for actual_phoneme in actual_phonemes
                          if actual_phoneme not in expected_phrase_phonemes]

        score = 100

        # scoring by training set
        phrase = get_expected_phrase_by_id(phrase_id)
        phonemes_training_set_weight_data = storage.stats[phrase]['phonemes_weight']

        for missing_phoneme in missing_phonemes:
            if missing_phoneme in phonemes_training_set_weight_data:
                weight = phonemes_training_set_weight_data[missing_phoneme]
                if weight > 0.4:
                    missing_phonemes_training_set[missing_phoneme] = weight

                if weight < 0.3:
                    continue

                weight = 1 - weight
                score = ((score * weight) * 0.165) + (score * 0.835)

        for extra_phonemes in missing_phonemes:
            if extra_phonemes in phonemes_training_set_weight_data:
                weight = phonemes_training_set_weight_data[extra_phonemes]
                if weight > 0.3:
                    extra_phonemes_training_set[extra_phonemes] = weight
                if weight < 0.3:
                    continue
                weight = 1 - weight
                score = ((score * weight) * 0.015) + (score * 0.985)

        phrase = get_expected_phrase_by_id(phrase_id)
        person_stats.phonemes_stats[phrase_id]['phrase'] = phrase
        person_stats.phonemes_stats[phrase_id]['total_phonemes_score'] = 0
        person_stats.phonemes_stats[phrase_id]['phonemes_score_by_training_set'] = {
            'phrase': phrase,
            'score': score,
            'actual_phonemes': actual_phonemes,
            'expected_phonemes': expected_phrase_phonemes,
            'missing_phonemes': missing_phonemes,
            'missing_phonemes_training_set': missing_phonemes_training_set,
            'extra_phonemes': extra_phonemes,
            'extra_phonemes_training_set': extra_phonemes_training_set,
        }
    except Exception as e:
        logger.error(e)

    return score


def get_score_for_phonemes_in_word(person_stats, phrase_id, word, expected_phonemes, actual_phonemes):
    """
    :param models.PersonStats.PersonStats person_stats:
    :return: return simple score for phonemes from 0 to 100
    :rtype: float
    """
    score = 0.0
    existing_phonemes_counter = 0
    try:
        missing_phonemes = list(expected_phonemes)
        removed_phonemes = {}
        for actual_phoneme in actual_phonemes:
            if actual_phoneme in expected_phonemes:
                if actual_phoneme in removed_phonemes:
                    continue
                removed_phonemes[actual_phoneme] = True
                missing_phonemes.remove(actual_phoneme)
                existing_phonemes_counter += 1

        total_expected_phrase_phonemes = len(expected_phonemes)
        expected_phrase_phonemes_percent_factor = 100.0 / float(total_expected_phrase_phonemes)
        missing_phonemes_score = 100 - (expected_phrase_phonemes_percent_factor * len(missing_phonemes))

        score = 0
        score += missing_phonemes_score

        person_stats.phonemes_stats[phrase_id]['phonemes_score_by_word'][word] = {
            'word': word,
            'score': score,
            'expected_phonemes': expected_phonemes,
            'missing_phonemes': missing_phonemes,
        }
    except Exception as e:
        logger.error(e)

    return score


def get_score_for_phonemes_in_phrase(phrase_id, person_stats):
    """
        calculate the simplest score for phonemes,
        calculate if the actual person phonemes exist in the expected phonemes.
        TODO: ignore extra phonemes from start and end.
        TODO: weight by training - get stats for extra/missing phonemes, if the phoneme is missing
              or substitute by a different phoneme, then use weight to calculate score.
              weight by training will make score more accurate with the training set data.
              our training data has phonemes stats of Israeli English speakers.
    :param phrase_id:
    :param models.PersonStats.PersonStats person_stats:
    :return: return simple score for phonemes from 0 to 100
    :rtype: float
    """
    score = 0.0
    score_by_words = 0.0
    score_by_words_total = 0.0
    existing_phonemes_counter = 0
    try:
        if phrase_id not in person_stats.phonemes_stats:
            person_stats.phonemes_stats[phrase_id] = {}

        person_stats.phonemes_stats[phrase_id]['phonemes_score_by_word'] = {}
        expected_phrase_data = get_expected_phrase_phonemes_by_phrase(phrase_id)
        expected_phrase_phonemes = []

        actual_phrase_data = person_stats.get_phrase_features_by_id(phrase_id)
        actual_phonemes = actual_phrase_data.pocketSphinksSoundFeatures.phonemes_list

        for word, phoneme_stats_list in expected_phrase_data.iteritems():
            phoneme_data_by_word = [phoneme_stats for phoneme_stats in phoneme_stats_list
                                    if phoneme_stats['score'] == 100][0]
            expected_phonemes_by_word = phoneme_data_by_word['phonemes']
            score_by_word = get_score_for_phonemes_in_word(person_stats,
                                                           phrase_id,
                                                           word,
                                                           expected_phonemes_by_word,
                                                           actual_phonemes)
            score_by_words_total += score_by_word
            map(expected_phrase_phonemes.append, expected_phonemes_by_word)

        score_by_words = score_by_words_total / float(len(expected_phrase_data))

        missing_phonemes = list(expected_phrase_phonemes)
        removed_phonemes = {}
        for actual_phoneme in actual_phonemes:
            if actual_phoneme in expected_phrase_phonemes:
                if actual_phoneme in removed_phonemes:
                    continue
                removed_phonemes[actual_phoneme] = True
                missing_phonemes.remove(actual_phoneme)
                existing_phonemes_counter += 1

        extra_phonemes = [actual_phoneme for actual_phoneme in actual_phonemes
                          if actual_phoneme not in expected_phrase_phonemes]

        total_expected_phrase_phonemes = len(expected_phrase_phonemes)
        expected_phrase_phonemes_percent_factor = 100.0 / float(total_expected_phrase_phonemes)
        missing_phonemes_score = 100 - (expected_phrase_phonemes_percent_factor * len(missing_phonemes))

        total_extra_phonemes = len(extra_phonemes)
        # extra phonemes normalization
        if total_extra_phonemes > MAX_EXTRA_PHONEMES:
            total_extra_phonemes = MAX_EXTRA_PHONEMES

        extra_phonemes_percent_factor = 100.0 / float(MAX_EXTRA_PHONEMES)
        extra_phonemes_score = 100 - (extra_phonemes_percent_factor * total_extra_phonemes)

        score = 0
        score += (MISSING_PHONEMES_WEIGHT_PERCENT * missing_phonemes_score)
        score += (EXTRA_PHONEMES_WEIGHT_PERCENT * extra_phonemes_score)
        score += (PHONEMES_SCORE_BY_WORDS_WEIGHT_PERCENT * score_by_words)
        phrase = get_expected_phrase_by_id(phrase_id)
        person_stats.phonemes_stats[phrase_id]['phrase'] = phrase
        person_stats.phonemes_stats[phrase_id]['phonemes_score_by_phrase'] = {
            'phrase': phrase,
            'score': score,
            'actual_phonemes': actual_phonemes,
            'expected_phonemes': expected_phrase_phonemes,
            'missing_phonemes': missing_phonemes,
            'extra_phonemes': extra_phonemes,
        }
    except Exception as e:
        logger.error(e)

    return score


def get_total_phoneme_score(storage, phrase_id, person_stats):
    score = 0
    test_data_simple_score = get_score_for_phonemes_in_phrase(phrase_id, person_stats)
    training_set_stats_score = get_phonemes_score_training_set(storage, phrase_id, person_stats)
    score += test_data_simple_score * 0.30
    score += training_set_stats_score * 0.70
    phonemes_phrase_data = person_stats.phonemes_stats[phrase_id]
    phonemes_score_by_training_set = phonemes_phrase_data["phonemes_score_by_training_set"]
    person_stats.detected_phonemes_substitutes = []

    for missing_phoneme in phonemes_score_by_training_set["missing_phonemes_training_set"].keys():
        person_stats.detected_phonemes_substitutes.append("Missing Phoneme: {}".format(missing_phoneme))

    for extra_phoneme in phonemes_score_by_training_set["extra_phonemes_training_set"].keys():
        if extra_phoneme in person_stats.detected_phonemes_substitutes:
            person_stats.detected_phonemes_substitutes.append("Extra Phoneme: {}".format(extra_phoneme))
        person_stats.detected_phonemes_substitutes.append("Extra Phoneme: {}".format(extra_phoneme))

    phonemes_phrase_data["total_phonemes_score"] = score
    return score
