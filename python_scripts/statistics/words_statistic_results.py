import logging

from statistics.phrases_data import phrase_to_say_1_simple, phrase_to_say_2_simple, phrase_to_say_3_simple, \
    get_expected_phrase_by_id, get_simple_phrase_by_id
from fuzzywuzzy import fuzz

logger = logging.getLogger('root')


def detect_closes_word(words_list=[], word=''):
    max_ratio = 0
    if not words_list:
        return ''
    close_word = words_list[0]

    for a_word in words_list:
        cur_ratio = fuzz.partial_ratio(word, a_word)
        if cur_ratio > max_ratio:
            max_ratio = cur_ratio
            close_word = a_word

    return close_word


def remove_less_sense_words(real_sentance=[], think_sentance=[]):
    if len(real_sentance) == len(think_sentance):
        return
    if len(real_sentance) < len(think_sentance):
        first_index = 0
        last_index = len(think_sentance) - 1
        if real_sentance[0] in think_sentance:
            first_index = think_sentance.index(real_sentance[0])
        if real_sentance[-1] in think_sentance:
            last_index = think_sentance.index(real_sentance[-1])
        think_sentance = think_sentance[first_index:last_index]


def detected_words_to_improve(person_stat=None, words_to_improve=[]):
    person_stat.words_stats['Sphinks_subs'] = []
    person_stat.words_stats['Google_subs'] = []
    cur_words_to_improve = set()
    detected_words_to_improve_sentance(person_stat.sound_features_phrase_1, cur_words_to_improve,
                                       phrase_to_say_1_simple, person_stat.words_stats)
    words_to_improve += cur_words_to_improve

    cur_words_to_improve = set()
    detected_words_to_improve_sentance(person_stat.sound_features_phrase_2, cur_words_to_improve,
                                       phrase_to_say_2_simple, person_stat.words_stats)
    words_to_improve += cur_words_to_improve

    cur_words_to_improve = set()
    detected_words_to_improve_sentance(person_stat.sound_features_phrase_3, cur_words_to_improve,
                                       phrase_to_say_3_simple, person_stat.words_stats)
    words_to_improve += cur_words_to_improve


def detected_words_to_improve_sentance(person_stat=None, words_to_improve=None, the_real_sentance='', store_dict={}):
    # assert isinstance(person_stat, PersonStats)

    '''
    print ("real list is ")
    print (the_real_sentance.split())
    print (person_stat.googleSoundFeatures.words_list)
    print (person_stat.pocketSphinksSoundFeatures.words_list)
    print
    '''

    real_sentance_google = the_real_sentance.split()
    real_sentance_sphinks = the_real_sentance.split()
    google_sentance = list(person_stat.googleSoundFeatures.words_list)
    sphinx_sentance = list(person_stat.pocketSphinksSoundFeatures.words_list)
    if not sphinx_sentance or not google_sentance:
        return
    remove_less_sense_words(the_real_sentance.split(), google_sentance)
    remove_less_sense_words(the_real_sentance.split(), sphinx_sentance)

    for word in the_real_sentance.split():
        if word in google_sentance:
            real_sentance_google.remove(word)
            google_sentance.remove(word)
        if word in sphinx_sentance:
            real_sentance_sphinks.remove(word)
            sphinx_sentance.remove(word)

    for word in list(reversed(real_sentance_google)):
        closest_word = detect_closes_word(google_sentance, word)
        sub = {}
        sub['word'] = word
        sub['sub'] = closest_word
        store_dict['Google_subs'].append(sub)
        words_to_improve.add(word)
        real_sentance_google.remove(word)
        if not closest_word == '':
            google_sentance.remove(closest_word)

    for word in list(reversed(real_sentance_sphinks)):
        closest_word = detect_closes_word(sphinx_sentance, word)
        sub = {}
        sub['word'] = word
        sub['sub'] = closest_word
        store_dict['Sphinks_subs'].append(sub)
        words_to_improve.add(word)
        real_sentance_sphinks.remove(word)
        if not closest_word == '':
            sphinx_sentance.remove(closest_word)


def english_accent_score_by_phrase(storage, person, phrase_id):
    score = 100
    try:
        phrase = get_expected_phrase_by_id(phrase_id)
        the_real_sentance = get_simple_phrase_by_id(phrase_id)
        number_of_words_in_sentance = len(the_real_sentance.split())
        real_sentance = the_real_sentance.split()
        sound_featuers = person.get_phrase_features_by_id(phrase_id)
        google_sentance = list(sound_featuers.googleSoundFeatures.words_list)
        sphinx_sentance = list(sound_featuers.pocketSphinksSoundFeatures.words_list)
        if not sphinx_sentance or not google_sentance:
            return

        for word in real_sentance:
            is_word_not_detected = False
            if word not in google_sentance:
                is_word_not_detected = True

            if word not in sphinx_sentance:
                is_word_not_detected = True

            if is_word_not_detected:
                if word in storage.stats[phrase]['words']:
                    score -= (100 - storage.stats[phrase]['words'][word]) / number_of_words_in_sentance
    except Exception as e:
        score = 0
        logger.error(e)
    return score
