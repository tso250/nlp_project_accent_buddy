import logging

from models.SoundFeatures import SoundFeatures
from models.SoundFeaturesPocketSphinks import SoundFeaturesPocketSphinks
from statistics.phrases_data import get_sentence_to_say_by_id, get_expected_phrase_by_id, \
    get_expected_phrase_phonemes_by_phrase, PHRASE_1_CAN_ID, PHRASE_2_SQUARE_ID, PHRASE_3_I_ID, article_phonemes_score

logger = logging.getLogger('root')
MISSING_BY_PHRASE_WEIGHT = 0.35
MISSING_BY_WORD_WEIGHT = 0.35

HIT_BY_PHRASE_WEIGHT = 0.13
HIT_BY_WORD_WEIGHT = 0.12

EXTRA_WEIGHT = 0.05

assert ((MISSING_BY_PHRASE_WEIGHT
         + MISSING_BY_WORD_WEIGHT
         + HIT_BY_PHRASE_WEIGHT
         + HIT_BY_WORD_WEIGHT
         + EXTRA_WEIGHT)
        == 1)


def scan_training_set_data_and_update_phonemes_stats_by_word(phonemes_phrase_stats,
                                                             actual_phonemes,
                                                             expected_words_phonemes):
    """

    :param phonemes_phrase_stats:
    :param actual_phonemes:
    :param expected_words_phonemes:
    :return:
    """
    try:
        phonemes_phrase_stats_expected = phonemes_phrase_stats['expected_by_word']
        phonemes_phrase_stats_missing = phonemes_phrase_stats['missing_by_word']
        phonemes_phrase_stats_existing = phonemes_phrase_stats['hit_by_word']
        phonemes_phrase_stats_amount = phonemes_phrase_stats['amount_by_word']

        missing_phonemes = list(expected_words_phonemes)
        removed_phonemes = {}

        for actual_phoneme in actual_phonemes:
            if actual_phoneme in expected_words_phonemes:
                if actual_phoneme in removed_phonemes:
                    continue
                removed_phonemes[actual_phoneme] = True
                missing_phonemes.remove(actual_phoneme)
                if actual_phoneme not in phonemes_phrase_stats_existing:
                    phonemes_phrase_stats_existing[actual_phoneme] = 0
                phonemes_phrase_stats_existing[actual_phoneme] += 1

        for missing_phoneme in missing_phonemes:
            if missing_phoneme not in phonemes_phrase_stats_missing:
                phonemes_phrase_stats_missing[missing_phoneme] = 0
            phonemes_phrase_stats_missing[missing_phoneme] += 1

        for expected_phoneme in expected_words_phonemes:
            if expected_phoneme not in phonemes_phrase_stats_expected:
                phonemes_phrase_stats_expected[expected_phoneme] = 0
            phonemes_phrase_stats_expected[expected_phoneme] += 1

        for phoneme in actual_phonemes:
            if phoneme in phonemes_phrase_stats_amount:
                phonemes_phrase_stats_amount[phoneme] += 1
            else:
                phonemes_phrase_stats_amount[phoneme] = 1

        phonemes_phrase_stats['expected_by_word'] = phonemes_phrase_stats_expected
        phonemes_phrase_stats['missing_by_word'] = phonemes_phrase_stats_missing
        phonemes_phrase_stats['hit_by_word'] = phonemes_phrase_stats_existing
        phonemes_phrase_stats['amount_by_word'] = phonemes_phrase_stats_amount
    except Exception as e:
        logger.error(e)


def scan_training_set_data_and_update_phonemes_stats(storage):
    """
    :param models.StorageModel.StorageModel storage:
    :param files_sound_features:
    :return:
    """

    phonemes_stats = {}
    try:
        for file_name in storage.sound_files_features:
            sound_feature = storage.sound_files_features[file_name]
            assert isinstance(sound_feature, SoundFeatures)

            phrase_key = sound_feature.get_phrase_id()

            if phrase_key not in phonemes_stats:
                phonemes_stats[phrase_key] = {}
                phonemes_phrase_stats = phonemes_stats[phrase_key]
                phonemes_phrase_stats['all_phonemes'] = {}
                phonemes_phrase_stats['expected'] = {}
                phonemes_phrase_stats['actual'] = {}
                phonemes_phrase_stats['hit'] = {}
                phonemes_phrase_stats['missing'] = {}
                phonemes_phrase_stats['extra'] = {}
                phonemes_phrase_stats['expected_by_word'] = {}
                phonemes_phrase_stats['amount_by_word'] = {}
                phonemes_phrase_stats['hit_by_word'] = {}
                phonemes_phrase_stats['missing_by_word'] = {}

            phonemes_phrase_stats = phonemes_stats[phrase_key]

            phonemes_phrase_stats_all_phonemes = phonemes_phrase_stats['all_phonemes']
            phonemes_phrase_stats_expected = phonemes_phrase_stats['expected']
            phonemes_phrase_stats_extra = phonemes_phrase_stats['extra']
            phonemes_phrase_stats_missing = phonemes_phrase_stats['missing']
            phonemes_phrase_stats_existing = phonemes_phrase_stats['hit']
            phonemes_phrase_stats_amount = phonemes_phrase_stats['actual']

            expected_phrase_phonemes = []
            expected_phrase_data = get_expected_phrase_phonemes_by_phrase(phrase_key)

            sphinks_features = sound_feature.pocketSphinksSoundFeatures
            assert isinstance(sphinks_features, SoundFeaturesPocketSphinks)
            logger.info("Sphinx recognized phonemes:" + str(sphinks_features.phonemes_list))

            actual_phonemes = sphinks_features.phonemes_list

            for word, phoneme_stats_list in expected_phrase_data.iteritems():
                phoneme_data_by_word = [phoneme_stats for phoneme_stats in phoneme_stats_list
                                        if phoneme_stats['score'] == 100][0]
                expected_phonemes_by_word = phoneme_data_by_word['phonemes']
                scan_training_set_data_and_update_phonemes_stats_by_word(phonemes_phrase_stats,
                                                                         actual_phonemes,
                                                                         expected_phonemes_by_word)
                map(expected_phrase_phonemes.append, expected_phonemes_by_word)

            missing_phonemes = list(expected_phrase_phonemes)
            removed_phonemes = {}
            for actual_phoneme in actual_phonemes:
                if actual_phoneme in expected_phrase_phonemes:
                    if actual_phoneme in removed_phonemes:
                        continue
                    removed_phonemes[actual_phoneme] = True
                    missing_phonemes.remove(actual_phoneme)
                    phonemes_phrase_stats_all_phonemes[actual_phoneme] = actual_phoneme
                    if actual_phoneme not in phonemes_phrase_stats_existing:
                        phonemes_phrase_stats_existing[actual_phoneme] = 0
                    phonemes_phrase_stats_existing[actual_phoneme] += 1

            for missing_phoneme in missing_phonemes:
                phonemes_phrase_stats_all_phonemes[missing_phoneme] = missing_phoneme
                if missing_phoneme not in phonemes_phrase_stats_missing:
                    phonemes_phrase_stats_missing[missing_phoneme] = 0
                phonemes_phrase_stats_missing[missing_phoneme] += 1

            extra_phonemes = [actual_phoneme for actual_phoneme in actual_phonemes
                              if actual_phoneme not in expected_phrase_phonemes]

            for extra_phoneme in extra_phonemes:
                phonemes_phrase_stats_all_phonemes[extra_phoneme] = extra_phoneme
                if extra_phoneme not in phonemes_phrase_stats_extra:
                    phonemes_phrase_stats_extra[extra_phoneme] = 0
                phonemes_phrase_stats_extra[extra_phoneme] += 1

            for expected_phoneme in expected_phrase_phonemes:
                phonemes_phrase_stats_all_phonemes[expected_phoneme] = expected_phoneme
                if expected_phoneme not in phonemes_phrase_stats_expected:
                    phonemes_phrase_stats_expected[expected_phoneme] = 0
                phonemes_phrase_stats_expected[expected_phoneme] += 1

            for phoneme in actual_phonemes:
                phonemes_phrase_stats_all_phonemes[phoneme] = phoneme
                if phoneme in phonemes_phrase_stats_amount:
                    phonemes_phrase_stats_amount[phoneme] += 1
                else:
                    phonemes_phrase_stats_amount[phoneme] = 1

    except Exception as e:
        logger.error(e)

    storage.sphinks['phonemes'] = phonemes_stats
    return phonemes_stats


def get_total_phrase_count(storage, phrase_id):
    sentence_to_say = get_sentence_to_say_by_id(phrase_id)
    return storage.sphinks['times'][sentence_to_say]


def determine_score(phonemes_phrase_stats, phoneme):
    weight = 0.0

    try:

        if phoneme not in phonemes_phrase_stats['missing']:
            total_missing = 0.0
        else:
            total_missing = phonemes_phrase_stats['missing'][phoneme]

        if phoneme not in phonemes_phrase_stats['hit']:
            total_hit = 0.0
        else:
            total_hit = phonemes_phrase_stats['hit'][phoneme]

        if phoneme not in phonemes_phrase_stats['expected']:
            total_expected = 0.0
        else:
            total_expected = phonemes_phrase_stats['expected'][phoneme]

        if phoneme not in phonemes_phrase_stats['actual']:
            total_actual = 0.0
        else:
            total_actual = phonemes_phrase_stats['actual'][phoneme]

        if phoneme not in phonemes_phrase_stats['extra']:
            total_extra = 0.0
        else:
            total_extra = phonemes_phrase_stats['extra'][phoneme]

        if phoneme not in phonemes_phrase_stats['missing_by_word']:
            total_missing_by_word = 0.0
        else:
            total_missing_by_word = phonemes_phrase_stats['missing_by_word'][phoneme]

        if phoneme not in phonemes_phrase_stats['hit_by_word']:
            total_hit_by_word = 0.0
        else:
            total_hit_by_word = phonemes_phrase_stats['hit_by_word'][phoneme]

        if phoneme not in phonemes_phrase_stats['expected_by_word']:
            total_expected_by_word = 0.0
        else:
            total_expected_by_word = phonemes_phrase_stats['expected_by_word'][phoneme]

        percent_missed = 0.0
        percent_hit = 0.0
        if total_expected != 0:
            percent_missed = (total_missing / float(total_expected))
            percent_hit = 1 - (total_hit / float(total_expected))

        percent_extra = 0.0
        if total_actual != 0:
            percent_extra = (total_extra / float(total_actual))

        percent_missed_by_word = 0.0
        percent_hit_by_word = 0.0
        if total_expected_by_word != 0:
            percent_missed_by_word = (total_missing_by_word / float(total_expected_by_word))
            percent_hit_by_word = (total_hit_by_word / float(total_expected_by_word))

        weight += percent_missed * MISSING_BY_PHRASE_WEIGHT
        weight += percent_missed_by_word * MISSING_BY_WORD_WEIGHT
        weight += percent_hit * HIT_BY_PHRASE_WEIGHT
        weight += percent_hit_by_word * HIT_BY_WORD_WEIGHT
        weight += percent_extra * EXTRA_WEIGHT

        if phoneme in article_phonemes_score:
            phoneme_article_score = article_phonemes_score[phoneme]
            weight = (weight * 0.6) + (phoneme_article_score * 0.4)

    except Exception as e:
        logger.error(e)

    return weight


def determine_score_for_phrase(storage, phonemes_stats, phrase_id):
    phonemes_weight = {}

    try:
        phrase = get_expected_phrase_by_id(phrase_id)
        if phrase not in storage.stats:
            storage.stats[phrase] = {}

        phonemes_phrase_stats = phonemes_stats[phrase_id]
        all_watched_phonemes = phonemes_phrase_stats['all_phonemes']
        for phoneme in all_watched_phonemes:
            try:
                phonemes_weight[phoneme] = determine_score(phonemes_phrase_stats, phoneme)
            except Exception as e:
                logger.error(e)

        storage.stats[phrase]['phonemes_weight'] = phonemes_weight
    except Exception as e:
        logger.error(e)


def calculate_phonemes_weight(storage):
    try:
        phonemes_stats = scan_training_set_data_and_update_phonemes_stats(storage)
        determine_score_for_phrase(storage, phonemes_stats, PHRASE_1_CAN_ID)
        determine_score_for_phrase(storage, phonemes_stats, PHRASE_2_SQUARE_ID)
        determine_score_for_phrase(storage, phonemes_stats, PHRASE_3_I_ID)
    except Exception as e:
        logger.error(e)
