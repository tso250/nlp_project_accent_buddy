import logging

from statistics.phrases_data import phrase_1_key, phrase_2_key, phrase_3_key
from statistics_settings import GOOGLE_RECOGNIZER_SCORE, SPHINKS_RECOGNIZER_SCORE

logger = logging.getLogger('root')

phrase_1_words = {
    'can': 0,
    'they': 0,
    'do': 0,
    'both': 0,
    'work': 0,
    'and': 0,
    'study': 0,
}

phrase_2_words = {
    'square': 0,
    'area': 0,
    'is': 0,
    'width': 0,
    'multiply': 0,
    'by': 0,
    'height': 0
}

phrase_3_words = {
    'i': 0,
    'can\'t': 0,
    'drink': 0,
    'cold': 0,
    'water': 0,
    'when': 0,
    'my': 0,
    'teeth': 0,
    'hurt': 0
}


def scan_training_set_data_and_update_words_stats(storage, files_sound_features):
    """
    run on first iteration
    weight: 0 - 1 (%)
    weight per word per recognizer
    'google':[{'word': 'work', 'amount': 5},
    {'word': 'can', 'amount': 15}]
    word - worst recognization percentage - highest weight.
    add 1 to a file contains the words that is recognized
    :param storage:
    :param files_sound_features:
    :return:
    """
    try:
        storage.google = {'times': {}, 'words': {}}

        storage.google['words'][phrase_1_key] = phrase_1_words.copy()
        storage.google['words'][phrase_2_key] = phrase_2_words.copy()
        storage.google['words'][phrase_3_key] = phrase_3_words.copy()

        storage.google['times'][phrase_1_key] = 0
        storage.google['times'][phrase_2_key] = 0
        storage.google['times'][phrase_3_key] = 0

        storage.sphinks = {
            'times': {}, 'words': {}
        }

        storage.sphinks['words'][phrase_1_key] = phrase_1_words.copy()
        storage.sphinks['words'][phrase_2_key] = phrase_2_words.copy()
        storage.sphinks['words'][phrase_3_key] = phrase_3_words.copy()

        storage.sphinks['times'][phrase_1_key] = 0
        storage.sphinks['times'][phrase_2_key] = 0
        storage.sphinks['times'][phrase_3_key] = 0

        for file_name in files_sound_features:
            sound_feature = files_sound_features[file_name]
            try:
                # add 1 to times sentence was
                storage.google['times'][sound_feature.sentence_to_say] += 1
                for google_word_stats in sound_feature.googleSoundFeatures.words_stats:
                    recognized_word = google_word_stats.word
                    percentage = google_word_stats.confidence

                    # ignore low confidence (optional)
                    if percentage < 0.5:
                        continue
                    logger.info("Google recognized :" + str(sound_feature.googleSoundFeatures.words_list))

                    if recognized_word in storage.google['words'][sound_feature.sentence_to_say]:
                        storage.google['words'][sound_feature.sentence_to_say][recognized_word] += 1

            except Exception as e:
                logger.error(e)

            try:
                # add 1 to times sentence was
                storage.sphinks['times'][sound_feature.sentence_to_say] += 1
                for sphinks_word_stats in sound_feature.pocketSphinksSoundFeatures.words_stats:
                    recognized_word = sphinks_word_stats.word
                    percentage = sphinks_word_stats.confidence
                    # ignore low confidence (optional)
                    if percentage < 0.2:
                        continue

                    logger.info("Sphinx recognized :" + str(sound_feature.pocketSphinksSoundFeatures.words_list))
                    if recognized_word in storage.sphinks['words'][sound_feature.sentence_to_say]:
                        storage.sphinks['words'][sound_feature.sentence_to_say][recognized_word] += 1

            except Exception as e:
                logger.error(e)

    except Exception as e:
        logger.error(e)


def determine_score(storage, word):
    try:
        total_score = GOOGLE_RECOGNIZER_SCORE + SPHINKS_RECOGNIZER_SCORE
        """
        determine_score is called
        only if the word does not exist in the expected phrase
        decide the score of the word according to times recognized
        :param word:
        :return:
        """
        sentence_to_say = None
        word = word.lower()
        if word in phrase_1_words:
            sentence_to_say = phrase_1_key
        elif word in phrase_2_words:
            sentence_to_say = phrase_2_key
        elif word in phrase_3_words:
            sentence_to_say = phrase_3_key

        # if word not in sentence it gets 0
        else:
            return 0

        # init (must not be 0)
        total_amount = 1
        if word in storage.google['words'][sentence_to_say]:
            total_amount = storage.google['times'][sentence_to_say]
            word_appearance = storage.google['words'][sentence_to_say][word]
        else:
            word_appearance = 0
        weight = word_appearance / float(total_amount)
        google_weight = weight * GOOGLE_RECOGNIZER_SCORE / float(total_score)

        if word in storage.sphinks['words'][sentence_to_say]:
            total_amount = storage.sphinks['times'][sentence_to_say]
            word_appearance = storage.sphinks['words'][sentence_to_say][word]
        else:
            word_appearance = 0
        weight = word_appearance / float(total_amount)
        sphinx_weight = weight * SPHINKS_RECOGNIZER_SCORE / float(total_score)
        ret_weight = google_weight + sphinx_weight
        return ret_weight * 100
    except Exception as e:
        logger.error(e)
        return 0


def determine_score_for_phrase(storage, phrase, phrase_words):
    try:
        if phrase not in storage.stats:
            storage.stats[phrase] = {}
            storage.stats[phrase]['words'] = {}
        total_words = 0
        total_score = 0

        logger.info("Determine Score for Phrase: " + phrase)
        for word in phrase_words:
            logger.info("Determine Score For word: " + word)
            word_score = determine_score(storage, word)
            total_words += 1
            total_score += word_score
            storage.stats[phrase]['words'][word] = word_score
            logger.info(word_score)

        if total_words > 0:
            total_score = total_score / float(total_words)

        storage.stats[phrase]['total_score'] = total_score
        storage.stats[phrase]['total_words'] = total_words
    except Exception as e:
        logger.error(e)
