import logging

from models.PersonStats import PersonStats
from sound_features_utils.sound_feature_extractor import extract_sound_features
from statistics.phonemes_score_by_statistics import get_total_phoneme_score
from statistics.phonemes_weight import calculate_phonemes_weight
from statistics.phrases_data import phrase_to_say_2, phrase_to_say_1, phrase_to_say_3, phrase_1_phonemes, \
    phrase_2_phonemes, phrase_3_phonemes, PHRASE_1_CAN_ID, PHRASE_2_SQUARE_ID, PHRASE_3_I_ID, get_expected_phrase_by_id
from statistics.words_statistic_results import detected_words_to_improve, english_accent_score_by_phrase
from statistics.words_weight_by_sentence import scan_training_set_data_and_update_words_stats, \
    determine_score_for_phrase

logger = logging.getLogger('root')


def calculate_score_by_person(storage, some_person_stats):
    try:
        if not some_person_stats:
            return None

        assert isinstance(some_person_stats, PersonStats)
        if not some_person_stats.phonemes_stats:
            some_person_stats.phonemes_stats = {}

        total_phonemes_score = 0
        total_phonemes_score_1 = get_total_phoneme_score(storage, PHRASE_1_CAN_ID, some_person_stats)
        total_phonemes_score_2 = get_total_phoneme_score(storage, PHRASE_2_SQUARE_ID, some_person_stats)
        total_phonemes_score_3 = get_total_phoneme_score(storage, PHRASE_3_I_ID, some_person_stats)

        total_phonemes_score += total_phonemes_score_1
        total_phonemes_score += total_phonemes_score_2
        total_phonemes_score += total_phonemes_score_3

        total_phrase_score_by_words_1 = english_accent_score_by_phrase(storage, some_person_stats, PHRASE_1_CAN_ID)
        total_phrase_score_by_words_2 = english_accent_score_by_phrase(storage, some_person_stats, PHRASE_2_SQUARE_ID)
        total_phrase_score_by_words_3 = english_accent_score_by_phrase(storage, some_person_stats, PHRASE_3_I_ID)

        phrase_1 = (total_phrase_score_by_words_1 * 0.4) + (total_phonemes_score_1 * 0.6)
        phrase_2 = (total_phrase_score_by_words_2 * 0.4) + (total_phonemes_score_2 * 0.6)
        phrase_3 = (total_phrase_score_by_words_3 * 0.4) + (total_phonemes_score_3 * 0.6)

        some_person_stats.phrases_stats = {get_expected_phrase_by_id(PHRASE_1_CAN_ID): phrase_1,
                                           get_expected_phrase_by_id(PHRASE_2_SQUARE_ID): phrase_2,
                                           get_expected_phrase_by_id(PHRASE_3_I_ID): phrase_3}

        some_person_stats.detected_sentences_to_improve = []
        total_score = 0

        phrase_score = 0
        total_phrases = 0
        for key in some_person_stats.phrases_stats:
            phrase_data = some_person_stats.phrases_stats[key]
            if phrase_data is None:
                continue
            phrase_score += phrase_data
            total_phrases += 1
            if phrase_data < 60:
                some_person_stats.detected_sentences_to_improve.append(key)

        if total_phrases > 0:
            phrase_score = phrase_score / float(total_phrases)

        total_score += phrase_score
        some_person_stats.total_person_score = total_score
    except Exception as e:
        some_person_stats.person_name = "bad_data"
        logger.error(e)
        return None
    return some_person_stats


def calculate_person_score(storage, person):
    if not person:
        return None

    if not calculate_score_by_person(storage, person):
        return None

    words_to_improve = []
    detected_words_to_improve(person, words_to_improve)
    assert isinstance(person, PersonStats)
    person.detected_words_to_improve = words_to_improve
    return person


def calculate_score_for_all_person(storage):
    for person_index in range(len(storage.persons_stats)):
        if not storage.persons_stats[person_index]:
            continue
        if "bad_data" in storage.persons_stats[person_index].person_name:
            storage.persons_stats[person_index] = None

    for person in storage.persons_stats:
        calculate_person_score(storage, person)


def calculate_statistics(storage):
    """
    :param models.StorageModel.StorageModel storage:
    :return:
    """

    extract_sound_features(storage)

    scan_training_set_data_and_update_words_stats(storage, storage.sound_files_features)
    calculate_phonemes_weight(storage)

    logger.info("Google Statistics:")
    logger.info(storage.google)
    logger.info("Sphinx Statistics:")
    logger.info(storage.sphinks)

    determine_score_for_phrase(storage, phrase_to_say_1, phrase_1_phonemes)
    determine_score_for_phrase(storage, phrase_to_say_2, phrase_2_phonemes)
    determine_score_for_phrase(storage, phrase_to_say_3, phrase_3_phonemes)


def show_persons_phrases_and_stats_test_set(storage):
    print ("\n\n\nShow all testing set statistics results per person:\n")
    try:
        for person_stats in storage.persons_stats:
            if person_stats is None:
                continue
            print ("\nShowing accent analysis for person {}".format(person_stats.person_name))
            calculate_person_score(storage, person_stats)
            print (person_stats)
    except Exception as e:
        logger.error(e)

    return None


def prepare_data_and_calculate_stats(storage):
    print("\nStep 1:\tGather all collected data from local storage...\n")
    print("\nStep 2:\tPreparing HMM model...\n")
    print("\nStep 3:\tLoading words dictionary for HMM model...\n")
    print("\nStep 4:\tLoading phonemes dictionary for HMM model...\n")
    print("\nStep 5:\tPreparing sound data...\n")
    print("\nStep 6:\tExtract features with CMU Pocket Sphinx model...\n")
    print("\nStep 7:\tExtract features with Google Speech Recognizer...\n")
    print("\nStep 8:\tCalculating statistics on training set data...\n")
    calculate_statistics(storage)
    print("\nFinished creating statistical scoring model!\n")
    print("\nStep 9:\tStore Training Data in storage...\n")
    print("\nStep 10:\tCalculate all person scores...\n")
    calculate_score_for_all_person(storage)
    print("")
    print("")
    print("")
