import logging

logger = logging.getLogger('root')

PHRASE_1_CAN_ID = 1
PHRASE_2_SQUARE_ID = 2
PHRASE_3_I_ID = 3


phrase_to_say_1 = 'Can they do both, work and study?'
phrase_to_say_2 = 'Square area is width multiply by height!'
phrase_to_say_3 = 'I can\'t drink cold water when my teeth hurt.'

phrase_to_say_1_simple = 'can they do both work and study'
phrase_to_say_2_simple = 'square area is width multiply by height'
phrase_to_say_3_simple = 'i can\'t drink cold water when my teeth hurt'

phrase_file_name_1 = 'can_they_do_both_work_and_study'
phrase_file_name_2 = 'square_area_is_width_multiply_by_height'
phrase_file_name_3 = 'i_cant_drink_water_when_my_teeth_hurt'

phrase_1_key = phrase_file_name_1.split('_')[0]
phrase_2_key = phrase_file_name_2.split('_')[0]
phrase_3_key = phrase_file_name_3.split('_')[0]

phrase_1_phonemes = {
    'can': [
        {'score': 100, 'phonemes': ['K', 'AE', 'N']},
        {'score': 100, 'phonemes': ['K', 'AH', 'N']},
        {'score': 90, 'phonemes': ['K', 'AE', 'NG']},
        {'score': 85, 'phonemes': ['K', 'AE', 'ER', 'NG']},
    ],
    'they': [
        {'score': 100, 'phonemes': ['DH', 'EY']}
    ],
    'do': [
        {'score': 100, 'phonemes': ['D', 'UW']}
    ],
    'both': [
        {'score': 100, 'phonemes': ['B', 'OW', 'TH']}
    ],
    'work': [
        {'score': 100, 'phonemes': ['W', 'ER', 'K']}
    ],
    'and': [
        {'score': 100, 'phonemes': ['AH', 'N', 'D']},
        {'score': 100, 'phonemes': ['AE', 'N', 'D']}
    ],
    'study': [
        {'score': 100, 'phonemes': ['S', 'T', 'AH', 'D', 'IY']}
    ]
}


phrase_2_phonemes = {
    'square': [
        {'score': 100, 'phonemes': ['S', 'K', 'W', 'EH', 'R']},
    ],
    'area': [
        {'score': 100, 'phonemes': ['EH', 'R', 'IY', 'AH']}
    ],
    'is': [
        {'score': 100, 'phonemes': ['IH', 'Z']}
    ],
    'width': [
        {'score': 100, 'phonemes': ['W', 'IH', 'D', 'TH']}
    ],
    'multiply': [
        {'score': 100, 'phonemes': ['M', 'AH', 'L', 'T', 'AH', 'P', 'L', 'AY']}
    ],
    'by': [
        {'score': 100, 'phonemes': ['B', 'AY']}
    ],
    'height': [
        {'score': 100, 'phonemes': ['HH', 'AY', 'T']}
    ]
}


phrase_3_phonemes = {
    'I': [
        {'score': 100, 'phonemes': ['AY']},
    ],
    'can\'t': [
        {'score': 100, 'phonemes': ['K', 'AE', 'N', 'T']}
    ],
    'drink': [

        {'score': 100, 'phonemes': ['D', 'R', 'IH', 'NG', 'K']}
    ],
    'cold': [
        {'score': 100, 'phonemes': ['K', 'OW', 'L', 'D']}
    ],
    'water': [
        {'score': 100, 'phonemes': ['W', 'AO', 'T', 'ER']}
    ],
    'when': [
        {'score': 100, 'phonemes': ['W', 'EH', 'N']},
        {'score': 100, 'phonemes': ['HH', 'W', 'EH', 'N']},
        {'score': 100, 'phonemes': ['W', 'IH', 'N']},
        {'score': 100, 'phonemes': ['HH', 'W', 'IH', 'N']}
    ],
    'my': [
        {'score': 100, 'phonemes': ['M', 'AY']}
    ],
    'teeth': [
        {'score': 100, 'phonemes': ['T', 'IY', 'TH']}
    ],
    'hurt': [
        {'score': 100, 'phonemes': ['HH', 'ER', 'T']}
    ]
}

phrase_data_by_id = {
    PHRASE_1_CAN_ID: phrase_1_phonemes,
    PHRASE_2_SQUARE_ID: phrase_1_phonemes,
    PHRASE_3_I_ID: phrase_1_phonemes,
}

phrase_by_id = {
    PHRASE_1_CAN_ID: phrase_to_say_1,
    PHRASE_2_SQUARE_ID: phrase_to_say_2,
    PHRASE_3_I_ID: phrase_to_say_3,
}

phrase_id_by_first_word = {
    phrase_1_key: PHRASE_1_CAN_ID,
    phrase_2_key: PHRASE_2_SQUARE_ID,
    phrase_3_key: PHRASE_3_I_ID,
}

phrase_first_word_by_id = {
    PHRASE_1_CAN_ID: phrase_1_key,
    PHRASE_2_SQUARE_ID: phrase_2_key,
    PHRASE_3_I_ID: phrase_3_key,
}

phrase_simple_word_by_id = {
    PHRASE_1_CAN_ID: phrase_to_say_1_simple,
    PHRASE_2_SQUARE_ID: phrase_to_say_2_simple,
    PHRASE_3_I_ID: phrase_to_say_3_simple,
}

article_phonemes_score = {
    'AA': 1,
    'AE': 1,
    'DH': 0.8,
    'TH': 0.8,
    'Z': 0.5,
    'S': 0.5,
}


def get_expected_phrase_phonemes_by_phrase(phrase_id):
    if phrase_id not in phrase_data_by_id:
        logger.fatal("Bad phrase ID[{}]".format(phrase_id))
        return None
    return phrase_data_by_id[phrase_id]


def get_expected_phrase_by_id(phrase_id):
    if phrase_id not in phrase_data_by_id:
        logger.fatal("Bad phrase ID[{}]".format(phrase_id))
        return None
    return phrase_by_id[phrase_id]


def get_phrase_id_by_sentence_to_say(sentence_to_say):
    if sentence_to_say not in phrase_id_by_first_word:
        logger.fatal("Bad sentence_to_say[{}]".format(sentence_to_say))
        return None
    return phrase_id_by_first_word[sentence_to_say]


def get_sentence_to_say_by_id(phrase_id):
    if phrase_id not in phrase_first_word_by_id:
        logger.fatal("Bad phrase_id[{}]".format(phrase_id))
        return None
    return phrase_first_word_by_id[phrase_id]


def get_simple_phrase_by_id(phrase_id):
    if phrase_id not in phrase_simple_word_by_id:
        logger.fatal("Bad phrase_id[{}]".format(phrase_id))
        return None
    return phrase_simple_word_by_id[phrase_id]