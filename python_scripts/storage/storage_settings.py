import os

MODELDIR = "../model/en-us"
DATADIR = "../sounds"
TESTING_SET_DIR = "../persons_sounds"
DATA_DIR_ANALYSIS = "../analysis"

STORAGE_PATH = "../statistics/storage.json"
sounds_dir = os.path.abspath(DATADIR)
testing_set_dir = os.path.abspath(TESTING_SET_DIR)
hmm_path = os.path.join(MODELDIR, 'acoustic-model')
dict_path = os.path.join(MODELDIR, 'pronounciation-dictionary.dict')
lm_path = os.path.join(MODELDIR, 'language-model.lm.bin')
phoneme_path = os.path.join('phonem-model.lm.bin')


def generate_unique_sound_file_name(phrase_file_name, uid):
    return phrase_file_name + '_' + uid + '.wav'
