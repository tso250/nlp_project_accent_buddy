import json
import logging
import os
from models.StorageModel import Storage
from statistics.phrases_data import phrase_1_key, phrase_2_key, phrase_3_key
from storage_settings import STORAGE_PATH
from statistics.words_weight_by_sentence import phrase_1_words, phrase_2_words, phrase_3_words

logger = logging.getLogger('root')


def init_storage():
    logger.info("start storage init...")

    storage = Storage({}, {}, {}, [])
    try:
        try:
            if os.path.isfile(STORAGE_PATH):
                with open(STORAGE_PATH, "r") as read_file:
                    json_object = json.load(read_file)
                    storage = Storage.from_json(json_object)
        except Exception as e:
            logger.error(e)

    except Exception as e:
        logger.error(e)

    if not storage.google:
        storage.google = {'times': {}, 'words': {}}

        storage.google['words'][phrase_1_key] = phrase_1_words.copy()
        storage.google['words'][phrase_2_key] = phrase_2_words.copy()
        storage.google['words'][phrase_3_key] = phrase_3_words.copy()

        storage.google['times'][phrase_1_key] = 0
        storage.google['times'][phrase_2_key] = 0
        storage.google['times'][phrase_3_key] = 0

        storage.sphinks = {
            'times': {}, 'words': {}
        }

        storage.sphinks['words'][phrase_1_key] = phrase_1_words.copy()
        storage.sphinks['words'][phrase_2_key] = phrase_2_words.copy()
        storage.sphinks['words'][phrase_3_key] = phrase_3_words.copy()

        storage.sphinks['times'][phrase_1_key] = 0
        storage.sphinks['times'][phrase_2_key] = 0
        storage.sphinks['times'][phrase_3_key] = 0

        logger.info("finished storage init...")

    return storage


def save_storage(storage):
    logger.info("start storage save...")
    try:
        with open(STORAGE_PATH, "w") as write_file:
            json_str = storage.to_json()
            write_file.write(json_str)
    except Exception as e:
        logger.error(e)
    logger.info("finished storage save...")


