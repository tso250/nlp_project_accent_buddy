from utils.log import setup_custom_logger
from statistics.stats_utils import prepare_data_and_calculate_stats, show_persons_phrases_and_stats_test_set
from storage.storage_utils import init_storage, save_storage
from utils.record_person import record_testing_set_data

logger = setup_custom_logger('root')


def create_csvs(storage):
    import csv
    stats = storage.stats
    for phrase in stats:
        phrase_stats = stats[phrase]
        phonemes_weight = phrase_stats["phonemes_weight"]
        words_weight = phrase_stats["words"]
        with open(phrase[0] + 'phonemes_weight' + '.csv', 'wb') as f:
            w = csv.DictWriter(f, phonemes_weight.keys())
            w.writeheader()
            w.writerow(phonemes_weight)

        with open(phrase[0] + 'words_weight' + '.csv', 'wb') as f:
            w = csv.DictWriter(f, words_weight.keys())
            w.writeheader()
            w.writerow(words_weight)


def demo():
    storage = init_storage()

    create_csvs(storage)

    prepare_data_and_calculate_stats(storage)

    show_persons_phrases_and_stats_test_set(storage)

    record_testing_set_data(storage)

    save_storage(storage)


demo()

