Step 1:	Gather all collected data from local storage...


Step 2:	Preparing HMM model...


Step 3:	Loading words dictionary for HMM model...


Step 4:	Loading phonemes dictionary for HMM model...


Step 5:	Preparing sound data...


Step 6:	Extract features with CMU Pocket Sphinks model...


Step 7:	Extract features with Google Speech Recognizer...


Step 8:	Calculating statistics on training set data...


Finished creating statistical scoring model!


Step 9:	Store Training Data in storage...


Step 10:	Calculate all person scores...


Record new data for testing set? y - yes, n - no
yes
Recording will start shortly...
Hello, please enter your name: 
Adam Cheriki
Please wait. Calibrating microphone...
...
Say: 'Can they do both, work and study?'
Please wait. Calibrating microphone...
...
Say: 'Square area is width multiply by height!'
Please wait. Calibrating microphone...
...
Say: 'I can't drink cold water when my teeth hurt.'
Extracting sound features for all recordings...
Finished 1 of 3
Finished 2 of 3
Finished 3 of 3
Hi Adam Cheriki, seems like we extracted all Google and CMU Pocket Sphinx sound features :)
Analyzing Adam Cheriki accent...
Person name:  Adam Cheriki 
	Total English Accent Score: # 65.76% #
	Detected Phrases To Improve: 
		*Square area is width multiply by height!*

	Detected Words To Improve: 

	Detected Phonemes To Improve: 
		*Missing Phoneme: B*
		*Missing Phoneme: AE*
		*Missing Phoneme: D*
		*Missing Phoneme: DH*
		*Missing Phoneme: AH*
		*Missing Phoneme: S*
		*Extra Phoneme: IY*
		*Extra Phoneme: B*
		*Extra Phoneme: AE*
		*Extra Phoneme: D*
		*Extra Phoneme: DH*
		*Extra Phoneme: AH*
		*Extra Phoneme: K*
		*Extra Phoneme: N*
		*Extra Phoneme: S*

	English Accent Score By Phrase:

		'can they do both work and study' got: # 74.71% #
		'square area is width multiply by height' got: # 55.51% #
		'i can't drink cold water when my teeth hurt' got: # 67.05% #
	
		Google thinks you said:
		{

	English Accent Score By Phonemes: 

	#		Can they do both, work and study?	
	#		Phrase simple phonemes score # 71.2% #	
	#		Training set phonemes score # 57.1% #	
	#		Total phrase phonemes score # 61.4% #	

	*and*
		Word 'and' phoneme score: # 100.0% #

	*do*
		Word 'do' phoneme score: # 100.0% #

	*both*
		Word 'both' phoneme score: # 66.67% #
		Missing Phonemes: TH 
		Expected Phonemes: B OW TH 

	*study*
		Word 'study' phoneme score: # 100.0% #

	*work*
		Word 'work' phoneme score: # 100.0% #

	*can*
		Word 'can' phoneme score: # 100.0% #

	*they*
		Word 'they' phoneme score: # 100.0% #

	#		Square area is width multiply by height!	
	#		Phrase simple phonemes score # 57.3% #	
	#		Training set phonemes score # 44.0% #	
	#		Total phrase phonemes score # 48.0% #	

	*and*
		Word 'and' phoneme score: # 100.0% #

	*do*
		Word 'do' phoneme score: # 100.0% #

	*both*
		Word 'both' phoneme score: # 66.67% #
		Missing Phonemes: TH 
		Expected Phonemes: B OW TH 

	*study*
		Word 'study' phoneme score: # 80.0% #
		Missing Phonemes: S 
		Expected Phonemes: S T AH D IY 

	*work*
		Word 'work' phoneme score: # 100.0% #

	*can*
		Word 'can' phoneme score: # 66.67% #
		Missing Phonemes: AE 
		Expected Phonemes: K AE N 

	*they*
		Word 'they' phoneme score: # 50.0% #
		Missing Phonemes: DH 
		Expected Phonemes: DH EY 

	#		I can't drink cold water when my teeth hurt.	
	#		Phrase simple phonemes score # 53.5% #	
	#		Training set phonemes score # 41.5% #	
	#		Total phrase phonemes score # 45.1% #	

	*and*
		Word 'and' phoneme score: # 100.0% #

	*do*
		Word 'do' phoneme score: # 100.0% #

	*both*
		Word 'both' phoneme score: # 66.67% #
		Missing Phonemes: B 
		Expected Phonemes: B OW TH 

	*study*
		Word 'study' phoneme score: # 60.0% #
		Missing Phonemes: S IY 
		Expected Phonemes: S T AH D IY 

	*work*
		Word 'work' phoneme score: # 100.0% #

	*can*
		Word 'can' phoneme score: # 66.67% #
		Missing Phonemes: AE 
		Expected Phonemes: K AE N 

	*they*
		Word 'they' phoneme score: # 50.0% #
		Missing Phonemes: DH 
		Expected Phonemes: DH EY 




Process finished with exit code 0
